package com.a65apps_tests.networking.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Specialty {
    @SerializedName("specialty_id")
    @Expose
    public Integer specialtyId;
    @SerializedName("name")
    @Expose
    public String name;
}
