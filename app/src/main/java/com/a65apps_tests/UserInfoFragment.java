package com.a65apps_tests;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserInfoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    final String TAG ="myLog";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public UserInfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserInfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserInfoFragment newInstance(String param1, String param2) {
        UserInfoFragment fragment = new UserInfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final String num = "102";

    /*  MutableLiveData<String> data = new MutableLiveData<>();
        //LiveData livedata =  new LiveData<String>();
        data.observe(getViewLifecycleOwner(), new Observer<String>(){
            @Override
            public void onChanged(String specialtyId) {
               String da = String.valueOf(App.getInstance().getDatabase().userDao().getSpec(num));
                Log.d(TAG, da);
            }
        });
  /*LiveData liveData = LiveData();
  liveData.setValue(App.getInstance().getDatabase().userDao().getSpec("102"));
  liveData.observe(getViewLifecycleOwner(), new Observer() {
      @Override
      public void onChanged(Object o) {

      }
  });*/


           /*   LiveData<List<User>> liveData = App.getInstance().getDatabase().userDao().getSpec(num);
                liveData.observe(getViewLifecycleOwner(), new Observer<List<User>>(){
                    @Override
                    public void onChanged(List<User> user) {
                        //user.get(1).id
                  //    Log.d(TAG, String.valueOf(user.size()));
                        //Log.d(TAG, user.specialtyId.get(0));
                     //   Log.d(TAG, "livedata = " + livedata.get());
                       /* if(user == null){
                            Log.d(TAG, "данных нет result");
                        }else {
                            Log.d(TAG, "данных есть result");
                            Log.d(TAG, String.valueOf(user.id));
                        }*/

              /*  });
            //  String name = "иВан";
       /* LiveData<User> liveData = App.getInstance().getDatabase().userDao().getNam(2);
        liveData.observe(getViewLifecycleOwner(), new Observer<User>(){
            @Override
            public void onChanged(User user) {
                //   Log.d(TAG, "livedata = " + livedata.get());
                if(user == null){
                    Log.d(TAG, "данных нет result");
                }else {
                    Log.d(TAG, "данных есть result");
                    Log.d(TAG, String.valueOf(user));
                }
            }
        });*/

           }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_info, container, false);
    }
}
