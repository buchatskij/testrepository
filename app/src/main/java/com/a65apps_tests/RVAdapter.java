package com.a65apps_tests;

import android.content.Context;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.PersonViewHolder> {

    public static Object OnItemClickListener;

    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        private  String Name;

        PersonViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
        }

        public void getData(){
            MutableLiveData<String> liveData = new MutableLiveData<>();
            liveData.postValue("fsdas");
        }
    }

    List<RVSpecialty> persons;

    public RVAdapter(List<RVSpecialty> persons){
        this.persons = persons;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {

        personViewHolder.name.setText(persons.get(i).name);

        personViewHolder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Context context = view.getContext();

              //  Name = persons.get(i).name;

                Toast.makeText(context, "вы выбрали " + persons.get(i).name, Toast.LENGTH_SHORT).show();

            }
        });

    }

    public interface OnItemClickListener<T> {
        void onItemClicked(int position, T item);
    }

    public void getData(){
        MutableLiveData<String> liveData = new MutableLiveData<>();
        liveData.postValue("fsdas");
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }
}