package com.a65apps_tests;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.a65apps_tests.model.User;
import com.a65apps_tests.model.UserSpecialty;
import com.a65apps_tests.viewModel.UserViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SpecialtiesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SpecialtiesFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

 //   private UserViewModel userViewModel;
    final String TAG ="myLog";
    WorkersFragment workersFragment;

    private  String []   SpecialArr;
    private RecyclerView rv;
    private List<RVSpecialty> persons;
    private String[] Name;
    private int siz;

    public SpecialtiesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SpecialtiesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SpecialtiesFragment newInstance(String param1, String param2) {
        SpecialtiesFragment fragment = new SpecialtiesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        UserViewModel userViewModel = new ViewModelProvider(getActivity()).get(UserViewModel.class);

        userViewModel.getAllUser().observe(getViewLifecycleOwner(), new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {

                int kol = users.size();
                String[] Special_name = new String[kol];
                for (int i = 0; i < kol; i++) {
                    Special_name[i] = users.get(i).getSpecialty_name();
                }

                Set<String> set = new HashSet<String>(Arrays.asList(Special_name));
                final String[] result = set.toArray(new String[set.size()]);


                rv=(RecyclerView) view.findViewById(R.id.rv);

                LinearLayoutManager llm = new LinearLayoutManager(getContext());
                rv.setLayoutManager(llm);
                rv.setHasFixedSize(true);

                siz = result.length;

                Log.d(TAG, String.valueOf(siz)+" ted");

                Name = new String[siz];

                for (int i = 0; i < siz; i++) {
                    Name[i] = result[i];

                }

                initializeData();
                initializeAdapter();

                //RVAdapter rvAdapter = new RVAdapter().persons;

              //  LiveData<String> liveData = RVAdapter
             /*


          /*      Spinner spinner = (Spinner) view.findViewById(R.id.Speciality);

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, result);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                spinner.post(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                           Log.d(TAG, result[position]);
                                // показываем позиция нажатого элемента
                              /*  int pos = result.length;
                                for (int i = 0; i<pos; i++) {
                                    Log.d(TAG, result[i] );

                                }

                               // textView.setText(spinner.getSelectedItem().toString());
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                               // textView.setText("");
                            }
                        });
                    }
                });*/

              /*  Spinner spinner = view.findViewById(R.id.Speciality);
                CustomAdapter adapter = new CustomAdapter(getContext(),
                        android.R.layout.simple_spinner_item, result);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        CustomAdapter.flag = false;
                        if(result[position] == null){
                            Log.d(TAG, "вы не выбрали значение");
                        }else {
                            CustomAdapter.flag = true;

                            Log.d(TAG, "вы выбрали значение !" + result[position]);
                        }
                        // Set adapter flag that something has been chosen


                    }

                      /*  getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_container, new WorkersFragment())
                                .addToBackStack(null)
                                .commit();*/


                    //  int pos = result.length;
                    //    for (int i = 0; i<pos; i++){
                        /*    if (position == i){
                                Log.d(TAG, "вы выбрали профессию " + result[i]);
                               /* int number = users.size();
                                for(int j = 0; j<number; j++) {
                                    if (result[i].equals(users.get(j).getSpecialty_name())) {
                                        getActivity().getSupportFragmentManager().beginTransaction()
                                                .replace(R.id.fragment_container, new WorkersFragment())
                                                .addToBackStack(null)
                                                .commit();


                                    } else {

                                }*/


                    //  }
          //      });


             /*   Spinner spinner = (Spinner) view.findViewById(R.id.Speciality);

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, result);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                spinner.setSelection(0);

                // устанавливаем обработчик нажатия
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onNothingSelected(AdapterView<?> parent, View view1) {
                    }

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id ) {
                        // показываем позиция нажатого элемента
                        int pos = result.length;
                        for (int i = 0; i<pos; i++){
                            if (position == i){
                                Log.d(TAG, "вы выбрали профессию " + result[i]);
                                int number = users.size();
                                for(int j = 0; j<number; j++) {
                                    if (result[i].equals(users.get(j).getSpecialty_name())) {
                                        getActivity().getSupportFragmentManager().beginTransaction()
                                                .replace(R.id.fragment_container, new WorkersFragment())
                                                .addToBackStack(null)
                                                .commit();

                                    } else {


                                        Log.d(TAG, users.get(j).getName());


                                      //  final FragmentTransaction replace =
                                              //  getActivity().getSupportFragmentManager().beginTransaction().replace(1, WorkersFragment.class);
                                        //    TabsAdapter tabsAdapter = new TabsAdapter(WorkersFragment.class, 2);
//
                                        //ragmentManager fragmentManager = getFragmentManager()
                                      //  FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                       /* FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                        transaction.replace(R.id.viewpager, workersFragment);
                                        transaction.commit();*/
                //       FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
                //   fragmentTransaction.replace(R.id.view_pager, WorkersFragment.FRAGMENT_ONE);


                //    Log.d(TAG, "вы выбрали профессию " + result[i]);
                //Log.d(TAG, " Вы выбрали профессию : " + result[i]);

                                 /*   if (result[i].equals(users.get(i).getSpecialty_name())) {
                                        Log.d(TAG, " Ураа!!! вы выбрали ту профессию которая соответствует пользователям  : "
                                                + users.get(i).getName());
                                    } else {
                                        Log.d(TAG, " У пользователя " + users.get(i).getName() + " другая профессия");
                                    }*/

                              /*  if(Special_name[i] == Special_name[0]){
                                    Log.d(TAG, " Вы выбрали профессию : " + Special_name[i] + " её ид равен " + Special_id[i]);
                                }else{
                                    Log.d(TAG, " Вы выбрали профессию : " + Special_name[i] + " её ид равен " + Special_id[i]);
                                }*/

                    /*        }
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                    }
                });*/
            }

        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_specialties, container, false);
    }

    private void initializeData(){

        persons = new ArrayList<>();

        String data1;

        for (int i = 0; i < siz; i++){
            data1 = Name[i];
            persons.add(new RVSpecialty(data1));
            Log.d(TAG,data1);
        }
    }

    private void initializeAdapter(){
        RVAdapter adapter = new RVAdapter(persons);
        rv.setAdapter(adapter);
    }


   /* @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    //    Button btnOpenSecond = getActivity().findViewById(R.id.btn_open_second);
    //    btnOpenSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Open second fragment
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new SecondFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        super.onActivityCreated(savedInstanceState);
    }*/
}
