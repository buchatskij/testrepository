package com.a65apps_tests;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class TabsAdapter extends FragmentStatePagerAdapter {

    final String TAG ="myLog";
    TabsAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Log.d(TAG, String.valueOf(position));
        switch (position) {
            case 0:
                return new SpecialtiesFragment();
            case 1:
                return new WorkersFragment();
            case 2:
                return new UserInfoFragment();

              default:
                return new SpecialtiesFragment();
        }
    }
}
