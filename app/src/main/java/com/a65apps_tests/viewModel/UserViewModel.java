package com.a65apps_tests.viewModel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.a65apps_tests.model.User;
import com.a65apps_tests.model.UserSpecialty;
import com.a65apps_tests.networking.model.Response;
import com.a65apps_tests.repository.UserRepository;

import java.util.List;

public class UserViewModel extends AndroidViewModel {


    private MutableLiveData<Response> mutableLiveData;
    private UserRepository userRepository;
    private LiveData<List<User>> AllUser;
    private LiveData<List<UserSpecialty>> AllSpecialty;

    public UserViewModel(Application application){
        super(application);
        userRepository = new UserRepository(application);
        AllUser = userRepository.getAllUser();
        AllSpecialty = userRepository.getAllSpecialty();

        if (mutableLiveData != null){
            return;
        }
        mutableLiveData = userRepository.getNews();
    }

    public LiveData<List<User>> getAllUser(){
        return AllUser;
    }

    public LiveData<List<UserSpecialty>> getAllSpecialty(){
        return AllSpecialty;
    }
}
