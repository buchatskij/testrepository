package com.a65apps_tests.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.List;

@Entity
public class SpecialtyUser {
    @PrimaryKey
 //   private long id;
    private long user_id;
   // @TypeConverters({specialtyIdConverter.class})
    private String specialtyId;
 //   @TypeConverters({nameConverter.class})
    private String name;

    public SpecialtyUser( @NonNull long user_id, @NonNull String specialtyId, @NonNull String name){  //@NonNull long id,
      //  this.id = id;
        this.user_id = user_id;
        this.specialtyId = specialtyId;
        this.name = name;
    }

  /*  @NonNull
    public long getId(){return this.id;}*/

    @NonNull
    public long getUser_id(){return this.user_id;}

    @NonNull
    public String getSpecialtyId(){return this.specialtyId;}

    @NonNull
    public String getName(){return this.name;}

}
