package com.a65apps_tests.model;

import androidx.room.TypeConverter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class nameConverter {
    @TypeConverter
    public String fromName(List<String> name) {
        return name.stream().collect(Collectors.joining(","));
    }

    @TypeConverter
    public List<String> toName (String data) {
        return Arrays.asList(data.split(","));
    }
}
