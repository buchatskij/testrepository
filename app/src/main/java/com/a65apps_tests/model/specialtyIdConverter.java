package com.a65apps_tests.model;

import androidx.room.TypeConverter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class specialtyIdConverter {
    @TypeConverter
    public String fromSpecialtyId(List<String> specialtyId) {
        return specialtyId.stream().collect(Collectors.joining(","));
    }

    @TypeConverter
    public List<String> toSpecialtyId(String data) {
        return Arrays.asList(data.split(","));
    }

}
