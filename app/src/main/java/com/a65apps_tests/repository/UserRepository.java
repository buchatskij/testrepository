package com.a65apps_tests.repository;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.a65apps_tests.AppsDatabase;
import com.a65apps_tests.model.SpecialtyUser;
import com.a65apps_tests.model.SpecialtyUserDao;
import com.a65apps_tests.model.User;
import com.a65apps_tests.model.UserDao;
import com.a65apps_tests.model.UserSpecialty;
import com.a65apps_tests.networking.RetrofitApi;
import com.a65apps_tests.networking.RetrofitService;
import com.a65apps_tests.networking.model.Example;
import com.a65apps_tests.networking.model.Response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class UserRepository {

    final String TAG = "myLog";
    private int id;
    private UserDao mUserDao;
    private SpecialtyUserDao mSpecialtyUserDao;
    private LiveData<List<User>> AllUser;
    private LiveData<List<UserSpecialty>> AllSpecialty;
    private RetrofitApi retrofitApi;
    private UserRepository userRepository;
    ArrayList<String> special_ID;
    ArrayList<String> special;
    int kol_vo = 0;


   public UserRepository getInstance() {
        if (userRepository == null) {
            userRepository = new UserRepository();
        }
        return userRepository;
    }

    public UserRepository() {
        retrofitApi = RetrofitService.cteateService(RetrofitApi.class);
    }

    public UserRepository(Application application) {
        AppsDatabase db = AppsDatabase.getAppsDatabase(application);
        mUserDao = db.userDao();
        AllUser = mUserDao.getAll();
        mSpecialtyUserDao = db.specialtyUserDao();
       // AllSpecialty = mSpecialtyUserDao.getUserSpecialty();
        retrofitApi = RetrofitService.cteateService(RetrofitApi.class);
    }

    public LiveData<List<User>> getAllUser() {
        return AllUser;
    }

    public LiveData<List<UserSpecialty>> getAllSpecialty(){

        return AllSpecialty;
    }

    public MutableLiveData<Response> getNews() {
        MutableLiveData<Response> newsData = new MutableLiveData<>();
        retrofitApi.example()
                .enqueue(new Callback<Example>() {
                             @Override
                             public void onResponse(Call<Example> call, retrofit2.Response<Example> response) {
                                 Example example = response.body();

                                 int siz  = example.response.size();

                                 for(int i = 0; i<siz; i++) {
                                     int chislo = example.response.get(i).specialty.size();
                                     kol_vo += chislo;}

                                 ArrayList<String> User_mass = new ArrayList<>();

                                 for(int i = 0; i<siz; i++) {
                                     int num = example.response.get(i).specialty.size();
                                     if(num>1){
                                         for(int l = 0; l<num; l++){
                                             User_mass.add(example.response.get(i).fName + "\t" + example.response.get(i).lName + "\t" +
                                                             example.response.get(i).birthday + "\t" + example.response.get(i).avatrUrl + "\t" +
                                                             example.response.get(i).specialty.get(l).specialtyId + "\t" + example.response.get(i).specialty.get(l).name);
                                         }
                                     }else{
                                         User_mass.add(example.response.get(i).fName + "\t" + example.response.get(i).lName + "\t" +
                                                 example.response.get(i).birthday + "\t" + example.response.get(i).avatrUrl + "\t" +
                                                 example.response.get(i).specialty.get(0).specialtyId + "\t" + example.response.get(i).specialty.get(0).name);

                                     }

                                     }

                                 Log.d(TAG, User_mass.size()+"");

                                 int num_user = User_mass.size();
                                 User users[] = new User[num_user];
                                 for(int q = 0; q<num_user; q++){
                                     String [] mass = User_mass.get(q).split("\t");
                                     users[q] = new User(q, mass[0],mass[1], mass[2], mass[3], mass[4],mass[5]);
                                 }
                                 insert(users);
                    }

                    @Override
                    public void onFailure(Call<Example> call, Throwable t) {
                       // newsData.setValue(null);
                    }
                });

        return newsData;
    }

    void insert(User[] users) {
        AppsDatabase.databaseWriteExecutor.execute(() -> {
            mUserDao.insertAll(Arrays.asList(users));
        });
    }
}